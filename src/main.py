from fastapi import FastAPI
from pydantic import BaseModel

from settings import APP_VERSION

app = FastAPI()


class Greeting(BaseModel):
    message: str


@app.get("/health")
async def healthcheck():
    return {'version': APP_VERSION}


@app.get("/hello/{username}")
async def get_task(username: str):
    return Greeting(message=f'Hello, {username}!')
